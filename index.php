<?php

//use Doctrine\ORM\Tools\Setup;
//use Doctrine\ORM\EntityManager;

require_once 'config/cli-config.php';
require_once 'src/Controller/DueDateCalculatorController.php';

$isDevMode = true;
$proxyDir = null;
$cache = null;
$useSimpleAnnotationReader = false;

//$config = Setup::createAnnotationMetadataConfiguration($paths, $isDevMode);
//$entityManager = EntityManager::create($dbParams, $config);


try {
    //$entityManager->getConnection()->connect();
} catch (\Exception $e) {
    echo ($e->getMessage());
}


$inputDateString = '2020-04-15 14:15:16';


$turnaroundTimeString = "1 day"; // means 24 * 60 minutes
$turnaroundTimeString = "3 dafdbdf"; // means 3 day -> 3 * 24 * 60 minutes
$turnaroundTimeString = "3 dfdbdf"; // means 3 minutes -> 3 minutes
$turnaroundTimeString = "3dfdbdf"; // means 3 minutes -> 3 minutes
$turnaroundTimeString = "dfdb3df"; // means 0 minutes -> 0 minutes
//$turnaroundTimeString = "dfdbdf"; // means 0 minutes -> 0 minutes
//$turnaroundTimeString = "134"; // means 134 minutes
//$turnaroundTimeString = "24 minute"; // means 24 minutes
//$turnaroundTimeString = "24 minutes"; // means 24 minutes
//$turnaroundTimeString = "1 week"; // means 7 * 24 * 60 minutes
//$turnaroundTimeString = "20 hour"; // means 20 * 60 minutes
$turnaroundTimeString = "480 minutes";

echo "inputDateString: ".$inputDateString."<br>";

$dueDateCalculator = new DueDateCalculatorController();
$outputDateString = $dueDateCalculator->calculate($inputDateString, $turnaroundTimeString);
echo "outputDateString: ".$outputDateString;