<?php

// Paths to Entities that we want Doctrine to see
$paths = array(
    "src/Entity"
);

// Tells Doctrine what mode we want
$isDevMode = true;

// Doctrine connection configuration
$dbParams = array(
    'driver' => 'pdo_mysql',
    'user' => 'sf4_user_test',
    'password' => 'sf4_pw_test',
    'dbname' => 'sf4_db_test'
);