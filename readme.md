Mellékeltem a szakmai leirást is:

Megoldásom sima natív PHP -ra esett.
composer van, de nincs használatban
autoloader van, de nincs használatban
Adatbázis és Entity manager van de nincs használatban
PHP Unit van

Docker konténer szolgáltatja a környezetet
docker compose fájl mellékeve
docker-compose up -d
login / enter docker:
 docker exec -it icftech-mysql mysql -usf4_user -psf4_pw

A két bemenő paraméter az index.php ban megadhatók:
- $inputDateString
- $turnaroundTimeString
(Input paraméter beviteli formájáról nem tesz említést a dokumentácio ezért a legegyszerűbb formát válaszottam. (Php változó paraméter))

A $turnaroundTimeString a következő módon:
$turnaroundTimeString = "1 day"; // means 24 * 60 minutes
$turnaroundTimeString = "3 dafdbdf"; // means 3 day -> 3 * 24 * 60 minutes
$turnaroundTimeString = "3 dfdbdf"; // means 3 minutes -> 3 minutes
$turnaroundTimeString = "3dfdbdf"; // means 3 minutes -> 3 minutes
$turnaroundTimeString = "dfdb3df"; // means 0 minutes -> 0 minutes
$turnaroundTimeString = "dfdbdf"; // means 0 minutes -> 0 minutes
$turnaroundTimeString = "134"; // means 134 minutes
$turnaroundTimeString = "24 minute"; // means 24 minutes
$turnaroundTimeString = "24 minutes"; // means 24 minutes
$turnaroundTimeString = "1 week"; // means 7 * 24 * 60 minutes
$turnaroundTimeString = "20 hour"; // means 20 * 60 minutes
$turnaroundTimeString = "480 minutes" // means 480 minutes

Tehát elfogad a rendszer:
 - percet, 
 - napot, 
 - hetet, 
 - hónapot 
 - és évet 
 is bemenő paraméternek.

Egyszerűen megjeleníti a képernyőn a céldátumot: $outputDateString néven.

A szabadság időpontjai tetszőlegesen beállítható újak hozzáadásával vagy a régiek elvételével.

Köszönöm a lehetőséget, hogy megoldhattam a tesztfeladatot
Üdvözlettel:Varga Ákos
achozion@gmail.com



Hmework

Due Date Calculator
Thanks for handing in your application to a developer position at Emarsys. With this
brief exercise we would like to assess your skills and capabilities on how you can
implement algorithms and write production codes.
The problem
We are looking for a solution that implements a due date calculator in an issue
tracking system. Your task is to implement the CalculateDueDate method:
•
•
Input: Takes the submit date/time and turnaround time.
Output: Returns the date/time when the issue is resolved.
Rules
•
•
•
•
•
Working hours are from 9AM to 5PM on every working day, Monday to Friday.
Holidays should be ignored (e.g. A holiday on a Thursday is considered as a
working day. A working Saturday counts as a non-working day.).
The turnaround time is defined in working hours (e.g. 2 days equal 16 hours).
If a problem was reported at 2:12PM on Tuesday and the turnaround time is
16 hours, then it is due by 2:12PM on Thursday.
A problem can only be reported during working hours. (e.g. All submit date
values are set between 9AM to 5PM.)
Do not use any third-party libraries for date/time calculations (e.g. Moment.js,
Carbon, Joda, etc.) or hidden functionalities of the built-in methods.
Additional info
•
•
•
•
•
Use your favourite programming language.
Do not implement the user interface or CLI.
Do not write a pseudo code. Write a code that you would commit/push to a
repository and which solves the given problem.
You have 24 hours to submit your solution.
You can submit your solution even if you have not finished it fully.
Bonus – Not mandatory
•
•
•
Including automated tests to your solution is a plus.
Test-driven (TDD) solutions are especially welcome.
Clean Code (by Robert. C. Martin) makes us happy.
If you have any further questions, don’t hesitate to ask.