<?php

namespace Emarsyshu\Controller;

/**
 * class BaseController
 *
 * @author Akos Varga <achozion@gmail.com>
 */
abstract class BaseController
{
    /**
     * @param string
     * @return integer
     */
    protected function getTurnaroundTimeInMinutes(string $turnaroundTime)
    {
        $explode = explode(" ", $turnaroundTime);
        $num = (integer) $explode[0];

        $return = $num;
        if (isset($explode[1])) {
            $firstTwoChar = strtolower(substr($explode[1], 0, 2));

            switch ($firstTwoChar) {
                case "mi":
                    break;
                case "ho": $return = $num * 60;
                    break;
                case "da": $return = $num * 60 * 24;
                    break;
                case "we": $return = $num * 60 * 24 * 7;
                    break;
                case "mo": $return = $num * 60 * 24 * 7 * 4;
                    break;
                case "ye": $return = $num * 60 * 24 * 7 * 4 * 12;
                    break;
            }
        }

        return (integer) $return;
    }

    /*
    * @param string
    * @param integer
    * @return integer
    */
    protected function calculateWorkingDays(string $inputDateString, $turnaroundTimeInMinutes)
    {
        $submitDatetime = \DateTime::createFromFormat('Y-m-d H:i:s', $inputDateString);
        $startDateString = $submitDatetime->format('Y-m-d H:i:s');

        $turnaroundTimeInDay = ceil($turnaroundTimeInMinutes / $this->oneWorkingDayInMinutes);
        $intvalSpec = "P".$turnaroundTimeInDay."D";
        $endDateTime = $submitDatetime->add(new \DateInterval($intvalSpec));
        $endDateString = $endDateTime->format('Y-m-d H:i:s');

        $workingDays = $this->getWorkingDays($startDateString, $endDateString);
        $workingDays--;
        return $workingDays;

    }

    /*
    * @param string
    * @param integer
    * @return string
    */
    protected function getMinutesUntilSameDayEnding(string $inputDateString, $turnaroundTimeInMinutes)
    {
        $submitDatetime = \DateTime::createFromFormat('Y-m-d H:i:s', $inputDateString);
        $submitDatetimeString = $submitDatetime->format('Y-m-d H:i:s');

        $sameDayEndDateString = $submitDatetime->format('Y-m-d 17:00:00');

        $time1 = new \DateTime($submitDatetimeString);
        $time2 = new \DateTime($sameDayEndDateString);

        $diff = $time1->diff($time2);
        $minutes = ($diff->days * 24 * 60) +  ($diff->h * 60) + $diff->i;

        return $minutes;
    }

    /*
    * @param string
    * @param string
    * @return integer
    */
    protected function getWorkingDays(string $startDateString, string $endDateString)
    {
        $endDate = strtotime($endDateString);
        $startDate = strtotime($startDateString);

        $days = ($endDate - $startDate) / 86400 + 1;
        $noFullWeeks = floor($days / 7);
        $noRemainingDays = fmod($days, 7);

        $theFirstDayOfWeek = date("N", $startDate);
        $theLastDayOfWeek = date("N", $endDate);

        if ($theFirstDayOfWeek <= $theLastDayOfWeek) {
            if ($theFirstDayOfWeek <= 6 && 6 <= $theLastDayOfWeek) $noRemainingDays--;
            if ($theFirstDayOfWeek <= 7 && 7 <= $theLastDayOfWeek) $noRemainingDays--;
        } else {
            if ($theFirstDayOfWeek == 7) {
                $noRemainingDays--;

                if ($theLastDayOfWeek == 6) {
                    $noRemainingDays--;
                }
            } else {
                $noRemainingDays -= 2;
            }
        }

        $workingDays = $noFullWeeks * 5;
        if ($noRemainingDays > 0) {
            $workingDays += $noRemainingDays;
        }

        foreach ($this->holidays as $holiday) {
            $time_stamp = strtotime(date('Y')."-".$holiday);
            if ($startDate <= $time_stamp && $time_stamp <= $endDate && date("N", $time_stamp) != 6 && date("N", $time_stamp) != 7)
                $workingDays--;
        }

        return $workingDays;
    }

}
